﻿using System;
using System.Linq;
using System.Reflection;
using System.Configuration;
using System.ComponentModel;
using System.Collections.Generic;

using PropertyChanged;

namespace OneOfZero.SwissArmyKnife.Configuration
{
	[ImplementPropertyChanged]
	public abstract class GenericSettings<T> : INotifyPropertyChanged where T : SettingsBase, new()
	{
		protected readonly IEnumerable<PropertyInfo> properties;

		protected readonly T container;

		public event PropertyChangedEventHandler PropertyChanged;

		public bool AutoSave { get; set; }

		public GenericSettings(bool autoload = false, bool autosave = false)
		{
			this.properties = this.GetType().GetProperties()
				.Where(p => p.GetCustomAttribute<SettingAttribute>(true) != null);

			this.container = (T)SettingsBase.Synchronized(new T());
			this.AutoSave = autosave;
			this.PropertyChanged += GenericSettings_PropertyChanged;

			if (autoload)
			{
				this.Load();
			}
		}

		public void Save()
		{
			foreach (PropertyInfo property in this.properties)
			{
				string name = this.GetSettingName(property);
				this.container[name] = property.GetValue(this);
			}

			this.container.Save();
		}

		public void Load()
		{
			this.PropertyChanged -= GenericSettings_PropertyChanged;
			foreach (PropertyInfo property in this.properties)
			{
				string name = this.GetSettingName(property);
				property.SetValue(this, this.container[name]);
			}
			this.PropertyChanged += GenericSettings_PropertyChanged;
		}

		protected void GenericSettings_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (this.AutoSave && this.properties.Any(p => p.Name == e.PropertyName))
			{
				this.Save();
			}
		}

		private string GetSettingName(PropertyInfo property)
		{
			SettingAttribute settingAttribute = property.GetCustomAttribute<SettingAttribute>(true);
			return String.IsNullOrEmpty(settingAttribute.Name) ? property.Name : settingAttribute.Name;
		}
	}

}
