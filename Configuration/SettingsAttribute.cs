﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneOfZero.SwissArmyKnife.Configuration
{
	[AttributeUsage(AttributeTargets.Property, Inherited = true)]
	public class SettingAttribute : Attribute
	{
		public string Name { get; set; }

		public SettingAttribute()
		{
		}

		public SettingAttribute(string name)
		{
			this.Name = name;
		}
	}
}
