﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace OneOfZero.SwissArmyKnife
{
	public static class GenericExtensions
	{
		/// <summary>
		/// Constant DateTime object used for calculation to and from Unix timestamps.
		/// </summary>
		private static readonly DateTime unixBase =
			new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);


		/// <summary>
		/// Constant array of byte factor units used for human readable size formatting.
		/// </summary>
		private static readonly string[] FactorUnits = new string[]	{
			"B", "kB", "MB", "GB", "TB", "PB"
		};

		/// <summary>
		/// Converts the provided <paramref name="dateTime"/> object to a Unix timestamp.
		/// </summary>
		/// <param name="dateTime">DateTime object</param>
		/// <returns>Unix timestamp</returns>
		public static long ToUnixTimestamp(this DateTime dateTime)
		{
			return (long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
		}

		/// <summary>
		/// Assumes the long value represents a Unix timestamp and converts it to a DateTime object.
		/// </summary>
		/// <param name="unixTimestamp">Unix timestamp</param>
		/// <returns>Date time object</returns>
		public static DateTime ToDateTime(this long unixTimestamp)
		{
			return unixBase.AddSeconds(unixTimestamp);
		}

		/// <summary>
		/// Assumes the long value represents a byte count and converts it to a human readable
		/// amount of bytes.
		/// </summary>
		/// <param name="byteCount">Byte count</param>
		/// <param name="format">String format used for the output. Argument 0 is a double typed
		/// value that is divided with the factor that makes the most readable value. Argument 1 
		/// is a string with the short unit notation.</param>
		/// <returns>Human readable amount of bytes</returns>
		public static string ToHumanBytes(this long byteCount, string format = "{0:F2}{1}")
		{
			return ((double)byteCount).ToHumanBytes();
		}

		/// <summary>
		/// Assumes the double value represents a byte count and converts it to a human readable
		/// amount of bytes.
		/// </summary>
		/// <param name="byteCount">Byte count</param>
		/// <param name="format">String format used for the output. Argument 0 is a double typed
		/// value that is divided with the factor that makes the most readable value. Argument 1 
		/// is a string with the short unit notation.</param>
		/// <returns>Human readable amount of bytes</returns>
		public static string ToHumanBytes(this double byteCount, string format = "{0:F2}{1}")
		{
			if (byteCount == 0)
				return String.Format(format, byteCount, FactorUnits[0]);

			int factor = (int)Math.Floor(Math.Log(byteCount, 1000));

			// Don't divide by zero...
			if (factor == 0)
				return String.Format(format, byteCount, FactorUnits[factor]);

			return String.Format(format, byteCount / Math.Pow(1000, factor), FactorUnits[factor]);
		}

		/// <summary>
		/// Attempts to make a binary clone of the provided object. Note that the object type must have the 
		/// [Serializable] attribute at a class level.
		/// </summary>
		/// <typeparam name="T">Type of the object to clone</typeparam>
		/// <param name="obj">Object to clone</param>
		/// <returns>Cloned object</returns>
		public static T BinaryClone<T>(this T obj) where T : ISerializable
		{
			if (typeof(T).GetCustomAttributes(typeof(SerializableAttribute), true) == null)
			{
				throw new ArgumentException(
					"To perform a binary clone, the object must have the [Serializable] attribute", "obj"
				);
			}

			using (MemoryStream stream = new MemoryStream())
			{
				BinaryFormatter formatter = new BinaryFormatter();
				formatter.Serialize(stream, obj);
				stream.Seek(0, SeekOrigin.Begin);
				return (T)formatter.Deserialize(stream);
			}
		}
	}
}
