﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Handlers;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace OneOfZero.SwissArmyKnife.Net
{
	public class AdvancedHttpClient : IDisposable
	{
		private HttpClientHandler _clientHandler;

		private ProgressMessageHandler _progressHandler;

		private HttpClient _client;

		public event EventHandler<HttpProgressEventArgs> HttpReceiveProgress
		{
			add { _progressHandler.HttpReceiveProgress += value; }
			remove { _progressHandler.HttpReceiveProgress -= value; }
		}

		public event EventHandler<HttpProgressEventArgs> HttpSendProgress
		{
			add { _progressHandler.HttpSendProgress += value; }
			remove { _progressHandler.HttpSendProgress -= value; }
		}

		public Uri BaseAddress
		{
			get { return _client.BaseAddress; }
			set { _client.BaseAddress = value; }
		}

		public CookieContainer Cookies
		{
			get { return _clientHandler.CookieContainer; }
			set { _clientHandler.CookieContainer = value; }
		}

		public HttpRequestHeaders Headers
		{
			get { return _client.DefaultRequestHeaders; }
		}

		public ClientCertificateOption ClientCertificateOptions
		{
			get { return _clientHandler.ClientCertificateOptions; }
			set { _clientHandler.ClientCertificateOptions = value; }
		}

		public bool AutomaticRedirect
		{
			get { return _clientHandler.AllowAutoRedirect; }
			set { _clientHandler.AllowAutoRedirect = value; }
		}

		public DecompressionMethods AutomaticDecompression
		{
			get { return _clientHandler.AutomaticDecompression; }
			set { _clientHandler.AutomaticDecompression = value; }
		}

		public ICredentials Credentials
		{
			get { return _clientHandler.Credentials; }
			set { _clientHandler.Credentials = value; }
		}

		public IWebProxy Proxy
		{
			get { return _clientHandler.Proxy; }
			set { _clientHandler.Proxy = value; }
		}

		public int MaxAutomaticRedirections
		{
			get { return _clientHandler.MaxAutomaticRedirections; }
			set { _clientHandler.MaxAutomaticRedirections = value; }
		}

		public long MaxRequestContentBufferSize
		{
			get { return _clientHandler.MaxRequestContentBufferSize; }
			set { _clientHandler.MaxRequestContentBufferSize = value; }
		}

		public long MaxResponseContentBufferSize
		{
			get { return _client.MaxResponseContentBufferSize; }
			set { _client.MaxResponseContentBufferSize = value; }
		}

		public bool PreAuthenticate
		{
			get { return _clientHandler.PreAuthenticate; }
			set { _clientHandler.PreAuthenticate = value; }
		}

		public bool UseCookies
		{
			get { return _clientHandler.UseCookies; }
			set { _clientHandler.UseCookies = value; }
		}

		public bool UseDefaultCredentials
		{
			get { return _clientHandler.UseDefaultCredentials; }
			set { _clientHandler.UseDefaultCredentials = value; }
		}

		public bool UseProxy
		{
			get { return _clientHandler.UseProxy; }
			set { _clientHandler.UseProxy = value; }
		}

		public TimeSpan Timeout
		{
			get { return _client.Timeout; }
			set { _client.Timeout = value; }
		}

		public JsonSerializerSettings JsonSerializerSettings { get; set; }

		public AdvancedHttpClient()
		{
			_clientHandler = new HttpClientHandler();
			_progressHandler = new ProgressMessageHandler(_clientHandler);
			_client = new HttpClient(_progressHandler);
		}

		public Task<HttpResponseMessage> GetAsync(string requestUri = "")
		{
			return _client.GetAsync(requestUri);
		}

		public Task<string> GetStringAsync(string requestUri = "")
		{
			return _client.GetStringAsync(requestUri);
		}

		public Task<T> GetObject<T>(string requestUri = "")
		{
			return _client
				.GetStringAsync(requestUri)
				.ContinueWith<T>(json => JsonConvert.DeserializeObject<T>(json.Result, JsonSerializerSettings))
			;
		}

		public Task<HttpResponseMessage> PostAsync(string requestUri = "", HttpContent content = null)
		{
			return _client.PostAsync(requestUri, content);
		}

		public Task<HttpResponseMessage> PostAsJsonAsync<T>(string requestUri = "", T value = default(T))
		{
			HttpContent content = new StringContent(JsonConvert.SerializeObject(value, JsonSerializerSettings));
			return _client.PostAsync(requestUri, content);
		}

		public Task<HttpResponseMessage> PutAsync(string requestUri = "", HttpContent content = null)
		{
			return _client.PutAsync(requestUri, content);
		}

		public Task<HttpResponseMessage> PutAsJsonAsync<T>(string requestUri = "", T value = default(T))
		{
			HttpContent content = new StringContent(JsonConvert.SerializeObject(value, JsonSerializerSettings));
			return _client.PutAsync(requestUri, content);
		}

		public Task<HttpResponseMessage> DeleteAsync(string requestUri = "")
		{
			return _client.DeleteAsync(requestUri);
		}

		public void Dispose()
		{
			_clientHandler.Dispose();
			_progressHandler.Dispose();
			_client.Dispose();
		}
	}
}
